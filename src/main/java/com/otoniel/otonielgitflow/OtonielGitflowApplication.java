package com.otoniel.otonielgitflow;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@SpringBootApplication
public class OtonielGitflowApplication {

	@Value("${build.version}")
	private String buildVersion;

	@RequestMapping("/")
    @ResponseBody
    String home() {
      return "This is a git flow Build: " + buildVersion +   " !";
    }

	public static void main(String[] args) {
		SpringApplication.run(OtonielGitflowApplication.class, args);
	}

}
