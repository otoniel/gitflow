#!/bin/bash

declare -r TRUE=0
declare -r FALSE=1
declare -r CURRENT_VERSION=$(xmllint --xpath "/*[local-name() = 'project']/*[local-name() = 'version']/text()" pom.xml)
declare -r CURRENT_VERSION_GAE=$(xmllint --xpath "/*[local-name() = 'project']/*[local-name() = 'properties']/*[local-name() = 'appengine.version']/text()" pom.xml)

# Current build version
declare -r RELEASE_VERSION=$(echo "$CURRENT_VERSION" | perl -pe 's/-SNAPSHOT//')
declare -r RELEASE_BRANCH=$(echo "$RELEASE_VERSION" | perl -pe 's{^([0-9]+)\.([0-9]+)\.([0-9]+)$}{"release/$1.$2"}e')

# Bellow helpers will increment BUILD version
declare -r BUILD_RELEASE_VERSION_INC=$(echo "$RELEASE_VERSION" | perl -pe 's{^([0-9]+)\.([0-9]+)\.([0-9]+)$}{"$1.$2." . ($3 + 1)}e')
declare -r BUILD_RELEASE_VERSION_NEXT="$(echo "$BUILD_RELEASE_VERSION_INC" | perl -pe 's/-SNAPSHOT//gi')-SNAPSHOT"
declare -r BUILD_RELEASE_VERSION_NEXT_GAE=$(echo "$BUILD_RELEASE_VERSION_INC" | perl -pe 's/\./\-/gi')

# Bellow helpers will increment MINOR version
declare -r MINOR_RELEASE_VERSION_INC=$(echo "$RELEASE_VERSION" | perl -pe 's{^([0-9]+)\.([0-9]+)\.([0-9]+)$}{"$1." . ($2 + 1) . ".0"}e')
declare -r MINOR_RELEASE_VERSION_NEXT="$(echo "$MINOR_RELEASE_VERSION_INC" | perl -pe 's/-SNAPSHOT//gi')-SNAPSHOT"
declare -r MINOR_RELEASE_VERSION_NEXT_GAE=$(echo "$MINOR_RELEASE_VERSION_INC" | perl -pe 's/\./\-/gi')

# Bellow helpers will increment MAJOR version
declare -r MAJOR_RELEASE_VERSION_INC=$(echo "$RELEASE_VERSION" | perl -pe 's{^([0-9]+)\.([0-9]+)\.([0-9]+)$}{($1 + 1) . ".0.0"}e')
declare -r MAJOR_RELEASE_VERSION_NEXT="$(echo "$MAJOR_RELEASE_VERSION_INC" | perl -pe 's/-SNAPSHOT//gi')-SNAPSHOT"

declare -r THREADS=1

echo ""
echo "BITBUCKET_BRANCH: $BITBUCKET_BRANCH"
echo "CURRENT_VERSION: $CURRENT_VERSION"
echo "RELEASE_VERSION: $RELEASE_VERSION"
echo "RELEASE_BRANCH: $RELEASE_BRANCH"
echo "MINOR_RELEASE_VERSION_INC: $MINOR_RELEASE_VERSION_INC"
echo "MINOR_RELEASE_VERSION_NEXT: $MINOR_RELEASE_VERSION_NEXT"
echo "MAJOR_RELEASE_VERSION_INC: $MAJOR_RELEASE_VERSION_INC"
echo "MAJOR_RELEASE_VERSION_NEXT: $MAJOR_RELEASE_VERSION_NEXT"
echo ""



./mvnw versions:set -DgenerateBackupPoms=false -DnewVersion=$RELEASE_VERSION 
git commit -a -m "Version bump to $RELEASE_VERSION"
git checkout -b release/${RELEASE_VERSION}
git push -u origin release/${RELEASE_VERSION}

git checkout develop
./mvnw versions:set -DgenerateBackupPoms=false -DnewVersion=$MINOR_RELEASE_VERSION_NEXT 
git commit -a -m "Version bump to $MINOR_RELEASE_VERSION_NEXT"
git push 


